package fr.minuskk.pvpswep.listener;

import fr.minuskk.pvpswep.PvpSwep;
import fr.minuskk.pvpswep.listener.commands.GameCommand;
import fr.minuskk.pvpswep.listener.events.JoinQuitListener;
import fr.minuskk.pvpswep.listener.events.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

public class Registerer {

    private PvpSwep plugin;

    public Registerer(PvpSwep plugin) {
        this.plugin = plugin;
    }

    public void registerEvents() {
        Listener[] listeners = {
                new JoinQuitListener(),
                new PlayerListener()
        };

        for(Listener listener : listeners)
            Bukkit.getPluginManager().registerEvents(listener, plugin);
    }

    public void registerCommands() {
        plugin.getCommand("game").setExecutor(new GameCommand());
    }

}
