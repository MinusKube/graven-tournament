package fr.minuskk.pvpswep.listener.events;

import fr.minuskk.pvpswep.Game;
import fr.minuskk.pvpswep.PvpSwep;
import fr.minuskk.pvpswep.SwepPlayer;
import fr.minuskk.pvpswep.util.CC;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import java.util.List;
import java.util.Random;

public class PlayerListener implements Listener {

    private Game game;

    public PlayerListener() {
        game = PvpSwep.game();
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if(!game.isStarted())
            e.setCancelled(true);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if(!game.isStarted())
            e.setCancelled(true);
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        if(!game.isStarted())
            e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if(!game.isStarted())
            e.setCancelled(true);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if(!game.isStarted())
            e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if(!game.isStarted() && e.getTo().getY() < 20) {
            e.getPlayer().teleport(PvpSwep.config().getLobbyLocation());
        }
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent e) {
        if(!game.isStarted())
            e.setCancelled(true);
    }


    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        Player player = e.getEntity();
        Player killer = player.getKiller();

        if(killer == null)
            e.setDeathMessage(CC.darkPurple + e.getEntity().getName() + CC.purple + " is dead!");
        else
            e.setDeathMessage(CC.darkPurple + e.getEntity().getName() + CC.purple + " has been killed by "
                    + CC.darkPurple + killer.getName() + CC.purple + "!");

        SwepPlayer.remove(player.getUniqueId());

        if(SwepPlayer.getPlayers().size() == 1)
            PvpSwep.game().end();
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        Player player = e.getPlayer();

        Random random = new Random();
        List<SwepPlayer> players = SwepPlayer.getPlayers();

        e.setRespawnLocation(players.get(random.nextInt(players.size())).getPlayer().getLocation());
        player.setGameMode(GameMode.SPECTATOR);
    }

}
