package fr.minuskk.pvpswep.listener.events;

import fr.minuskk.pvpswep.PvpSwep;
import fr.minuskk.pvpswep.SwepPlayer;
import fr.minuskk.pvpswep.util.CC;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinQuitListener implements Listener {

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent e) {
        if(Bukkit.getOnlinePlayers().size() >= 8 || PvpSwep.game().isStarted())
            e.setResult(PlayerLoginEvent.Result.KICK_FULL);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        e.setJoinMessage(CC.gold + e.getPlayer().getName() + CC.yellow + " joined the game!");

        Player p = e.getPlayer();

        p.setGameMode(GameMode.ADVENTURE);
        p.setExp(0);
        p.setLevel(0);

        p.teleport(PvpSwep.config().getLobbyLocation());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        e.setQuitMessage(CC.darkRed + e.getPlayer().getName() + CC.red + " left the game!");

        SwepPlayer.remove(e.getPlayer().getUniqueId());
    }

}
