package fr.minuskk.pvpswep.listener.commands;

import fr.minuskk.pvpswep.Game;
import fr.minuskk.pvpswep.PvpSwep;
import fr.minuskk.pvpswep.util.CC;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class GameCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(args.length == 0) {
            sender.sendMessage(CC.yellow + "Commands list:");
            sender.sendMessage(CC.gray + "  - /" + label + " start <time>");
            sender.sendMessage(CC.gray + "  - /" + label + " stop");

            return true;
        }

        Game game = PvpSwep.game();

        if(args[0].equalsIgnoreCase("start")) {
            if(args.length != 2) {
                sender.sendMessage(CC.darkRed + "Unknown syntax! " + CC.red + "/" + label + " start <time>");
                return true;
            }

            if(game.isLaunched() || game.isStarted()) {
                sender.sendMessage(CC.red + "The game is already started...");
                return true;
            }

            try {
                int input = Math.max(0, Integer.parseInt(args[1]));
                game.launch(input);

                sender.sendMessage(CC.green + "The game has been launched!");
            } catch(NumberFormatException e) {
                sender.sendMessage(CC.darkRed + "The <time> argument must be a number!");
                return true;
            }
        }

        else if(args[0].equalsIgnoreCase("stop")) {
            if(!game.isLaunched() || !game.isStarted()) {
                sender.sendMessage(CC.red + "The game is not launched or started...");
                return true;
            }

            if(game.isStarted())
                game.end();
            else
                game.unlaunch();

            sender.sendMessage(CC.green + "The game has been stopped!");
            return true;
        }

        return true;
    }

}
