package fr.minuskk.pvpswep;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.*;

public class SwepPlayer {

    private static final Map<UUID, SwepPlayer> SWEP_PLAYERS = new HashMap<>();

    private UUID uuid;

    private SwepPlayer(UUID uuid) {
        this.uuid = uuid;
    }

    public static List<SwepPlayer> getPlayers() { return new ArrayList<>(SWEP_PLAYERS.values()); }

    public UUID getUUID() { return uuid; }
    public Player getPlayer() { return Bukkit.getPlayer(uuid); }

    public static SwepPlayer instanceOf(UUID uuid) { return SWEP_PLAYERS.get(uuid); }
    public static SwepPlayer instanceOf(Player player) { return instanceOf(player.getUniqueId()); }

    public static SwepPlayer add(UUID uuid) { return SWEP_PLAYERS.put(uuid, new SwepPlayer(uuid)); }
    public static SwepPlayer remove(UUID uuid) { return SWEP_PLAYERS.remove(uuid); }

}
