package fr.minuskk.pvpswep;

import fr.minuskk.pvpswep.tasks.RandomTPTask;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;

public class Game {

    private RandomTPTask tpTask;

    private Countdown countdown;

    private boolean launched = false;
    private boolean started = false;
    private boolean ended = false;

    public Game() {
        tpTask = new RandomTPTask(PvpSwep.instance());
    }

    public void launch(int time) {
        if(launched || started)
            return;

        if(countdown != null)
            countdown.cancel();

        launched = true;

        countdown = new Countdown(this, time);
        countdown.runTaskTimer(PvpSwep.instance(), 20, 20);
    }

    public void unlaunch() {
        launched = false;

        if(countdown != null)
            countdown.cancel();

        countdown = null;
    }

    public void start() {
        if(started)
            return;

        launched = false;
        started = true;
        countdown = null;

        Bukkit.getOnlinePlayers().forEach(p -> {
            SwepPlayer.add(p.getUniqueId());

            p.setExp(0);
            p.setLevel(0);
            p.setGameMode(GameMode.SURVIVAL);
            p.getInventory().clear();

            p.setFoodLevel(20);
            p.setHealth(20);
        });

        tpTask.runTaskTimer(PvpSwep.instance(), 0, 60 * 20);
    }

    public void end() {
        if(!started || ended)
            return;

        Bukkit.broadcastMessage("The game is ended!");

        started = false;
        ended = true;
    }

    public boolean isLaunched() { return launched; }
    public boolean isStarted() { return started; }
    public boolean isEnded() { return ended; }

}
