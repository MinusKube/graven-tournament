package fr.minuskk.pvpswep.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Probabilities<T> {

    private static Random random = new Random();

    private Map<T, Float> items = new HashMap<>();

    public void add(T item, float prob) { items.put(item, prob); }
    public void remove(T item) { items.remove(item); }

    public Map<T, Float> getItems() { return new HashMap<>(items); }

    public T randomItem() {
        float total = 0;

        for (float proba : items.values())
            total += proba;

        float rf = random.nextFloat();
        rf *= total;

        float cur = 0;
        for(T item : items.keySet()) {
            if(item == null)
                continue;

            Float proba = items.get(item);

            if(proba == null)
                continue;

            if(rf >= cur && rf < cur + proba)
                return item;

            cur += proba;
        }

        return items.keySet().stream().findFirst().orElse(null);
    }

}
