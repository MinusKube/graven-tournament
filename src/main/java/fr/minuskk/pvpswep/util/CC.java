package fr.minuskk.pvpswep.util;

import org.bukkit.ChatColor;

public class CC {

    public static String red = ChatColor.RED.toString();
    public static String darkRed = ChatColor.DARK_RED.toString();
    public static String aqua = ChatColor.AQUA.toString();
    public static String darkAqua = ChatColor.DARK_AQUA.toString();
    public static String darkBlue = ChatColor.DARK_BLUE.toString();
    public static String blue = ChatColor.BLUE.toString();
    public static String gold = ChatColor.GOLD.toString();
    public static String black = ChatColor.BLACK.toString();
    public static String gray = ChatColor.GRAY.toString();
    public static String darkGray = ChatColor.DARK_GRAY.toString();
    public static String green = ChatColor.GREEN.toString();
    public static String darkGreen = ChatColor.DARK_GREEN.toString();
    public static String purple = ChatColor.LIGHT_PURPLE.toString();
    public static String darkPurple = ChatColor.DARK_PURPLE.toString();
    public static String yellow = ChatColor.YELLOW.toString();
    public static String white = ChatColor.WHITE.toString();

}
