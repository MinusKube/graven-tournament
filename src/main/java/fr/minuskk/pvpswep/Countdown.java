package fr.minuskk.pvpswep;

import fr.minuskk.pvpswep.util.CC;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

public class Countdown extends BukkitRunnable {

    private Game game;

    private int countdown;
    private int time;

    public Countdown(Game game, int time) {
        this.game = game;
        this.time = this.countdown = time;
    }

    @Override
    public void run() {
        if(!game.isLaunched() || game.isStarted()) {
            cancel();
            return;
        }

        Bukkit.getOnlinePlayers().forEach(p -> p.setExp(1 - (time / (float) countdown)));

        if(time == 0) {
            game.start();

            cancel();
            return;
        }

        if(time % 30 == 0 || time < 5) {
            Bukkit.broadcastMessage(CC.gold + "Game starting in " + CC.yellow + time
                    + CC.gold + " second" + (time > 1 ? "s." : "."));
        }

        time--;
    }

    public int getCountdown() { return countdown; }
    public int getTime() { return time; }

}
