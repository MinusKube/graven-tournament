package fr.minuskk.pvpswep.tasks;

import fr.minuskk.pvpswep.PvpSwep;
import fr.minuskk.pvpswep.util.Probabilities;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class ChestRefillTask implements Runnable {

    private static final Probabilities<ItemStack> PROBABILITIES = new Probabilities<ItemStack>() {
        {
            add(new ItemStack(Material.BAKED_POTATO), 20);
            add(new ItemStack(Material.COOKED_BEEF), 21);
            add(new ItemStack(Material.COOKED_FISH), 22);
            add(new ItemStack(Material.IRON_SWORD), 15);
            add(new ItemStack(Material.IRON_AXE), 16);
            add(new ItemStack(Material.IRON_PICKAXE), 17);
            add(new ItemStack(Material.IRON_BOOTS), 5);
            add(new ItemStack(Material.IRON_CHESTPLATE), 6);
            add(new ItemStack(Material.IRON_HELMET), 9);
            add(new ItemStack(Material.IRON_LEGGINGS), 11);
            add(new ItemStack(Material.LEATHER_HELMET), 12);
            add(new ItemStack(Material.LEATHER_CHESTPLATE), 13);
            add(new ItemStack(Material.LEATHER_BOOTS), 14);
            add(new ItemStack(Material.LEATHER_LEGGINGS), 15);
            add(new ItemStack(Material.ARROW), 10);
            add(new ItemStack(Material.BOW), 4);
            add(new ItemStack(Material.COBBLESTONE), 16);
            add(new ItemStack(Material.WATER_BUCKET), 18);
            add(new ItemStack(Material.LAVA_BUCKET), 17);
            add(new ItemStack(Material.WEB), 7);
            add(new ItemStack(Material.DIAMOND_HELMET), 2);
            add(new ItemStack(Material.DIAMOND_CHESTPLATE), 1);
            add(new ItemStack(Material.DIAMOND_LEGGINGS), 1);
            add(new ItemStack(Material.DIAMOND_BOOTS), 2);
            add(new ItemStack(Material.IRON_INGOT), 7);
            add(new ItemStack(Material.DIAMOND), 1);
        }
    };

    @Override
    public void run() {
        World world = Bukkit.getWorlds().get(0);
        Random random = new Random();

        PvpSwep.config().getIslandLocations().forEach(loc -> {
            int cx = loc.getChunk().getX();
            int cz = loc.getChunk().getZ();

            final int radius = 100 / 16;

            for(int x = -radius; x <= radius; x++) {
                for(int z = -radius; z <= radius; z++) {
                    Chunk chunk = world.getChunkAt(cx + x, cz + z);

                    for(BlockState bs : chunk.getTileEntities()) {
                        if(!(bs instanceof Chest))
                            continue;

                        Chest chest = (Chest) bs;

                        Inventory inv = chest.getBlockInventory();
                        inv.clear();

                        int ri = random.nextInt(10) + 8;
                        for(int i = 0; i < ri; i++) {
                            int slot;

                            do {
                                slot = random.nextInt(inv.getSize());
                            } while(inv.getItem(slot) != null);

                            ItemStack item = PROBABILITIES.randomItem();

                            if(item.getMaxStackSize() >= 2)
                                item.setAmount(random.nextInt(item.getMaxStackSize() / 2));

                            if(random.nextInt(3) == 0 && item.getType().name().contains("SWORD"))
                                item.addEnchantment(random.nextBoolean() ? Enchantment.DAMAGE_ALL : Enchantment.KNOCKBACK, 1);

                            inv.setItem(slot, item);
                        }
                    }
                }
            }
        });
    }

}
