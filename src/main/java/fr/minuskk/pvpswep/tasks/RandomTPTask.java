package fr.minuskk.pvpswep.tasks;

import fr.minuskk.pvpswep.PvpSwep;
import fr.minuskk.pvpswep.SwepPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomTPTask extends BukkitRunnable {

    private static final Random RANDOM = new Random();

    private int time = 0;
    private PvpSwep plugin;

    public RandomTPTask(PvpSwep plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        List<SwepPlayer> players = SwepPlayer.getPlayers();

        for(SwepPlayer sp : players) {
            Player p = sp.getPlayer();
            p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 120, 2));
        }

        List<Location> locationsToTp;

        if(++time == 2)
            locationsToTp = new ArrayList<>(PvpSwep.config().getLastLocations());
        else
            locationsToTp = new ArrayList<>(PvpSwep.config().getSpawnLocations());

        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            new ChestRefillTask().run();

            for(SwepPlayer sp : players) {
                Player p = sp.getPlayer();
                Location loc = locationsToTp.get(RANDOM.nextInt(locationsToTp.size()));

                p.teleport(loc);

                locationsToTp.remove(loc);
            }
        }, 60);

        if(time == 2)
            cancel();
    }

}
