package fr.minuskk.pvpswep;

import fr.minuskk.pvpswep.config.Config;
import fr.minuskk.pvpswep.listener.Registerer;
import org.bukkit.plugin.java.JavaPlugin;

public class PvpSwep extends JavaPlugin {

    private static PvpSwep instance;
    private static Config config;

    private static Game game;

    @Override
    public void onEnable() {
        instance = this;

        config = new Config(this);
        game = new Game();

        Registerer registerer = new Registerer(this);
        registerer.registerCommands();
        registerer.registerEvents();
    }

    @Override
    public void onDisable() {
    }

    public static PvpSwep instance() { return instance; }
    public static Config config() { return config; }
    public static Game game() { return game; }

}
