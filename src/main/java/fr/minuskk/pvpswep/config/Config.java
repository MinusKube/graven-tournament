package fr.minuskk.pvpswep.config;

import fr.minuskk.pvpswep.PvpSwep;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

public class Config {

    private PvpSwep plugin;

    private List<Location> spawnLocations = new ArrayList<>();
    private Location lobbyLocation;
    private List<Location> lastSpawnLocations = new ArrayList<>();
    private List<Location> islandLocations = new ArrayList<>();


    public Config(PvpSwep instance) {
        plugin = instance;

        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();

        load();
    }

    public void load() {
        spawnLocations.clear();

        List<String> spawnLocationsString = plugin.getConfig().getStringList("spawnLocations");

        for(String location : spawnLocationsString) {
            String[] splitted = location.split(",");
            spawnLocations.add(new Location(Bukkit.getWorlds().get(0), Integer.parseInt(splitted[0]),
                    Integer.parseInt(splitted[1]), Integer.parseInt(splitted[2])));
        }

        //------------------------------------

        String[] lobbyLocationString = plugin.getConfig().getString("lobbyLocation").split(",");

        lobbyLocation = new Location(Bukkit.getWorlds().get(0), Integer.parseInt(lobbyLocationString[0]),
                Integer.parseInt(lobbyLocationString[1]), Integer.parseInt(lobbyLocationString[2]));

        //------------------------------------

        lastSpawnLocations.clear();

        List<String> lastSpawnLocationsString = plugin.getConfig().getStringList("lastLocations");

        for(String location : lastSpawnLocationsString) {
            String[] splitted = location.split(",");
            lastSpawnLocations.add(new Location(Bukkit.getWorlds().get(0), Integer.parseInt(splitted[0]),
                    Integer.parseInt(splitted[1]), Integer.parseInt(splitted[2])));
        }

        System.out.println("MaxPlayer: " + Math.min(spawnLocations.size(), lastSpawnLocations.size()));

        //------------------------------------

        islandLocations.clear();

        List<String> islandLocationsString = plugin.getConfig().getStringList("islands");

        for(String location : islandLocationsString) {
            String[] splitted = location.split(",");
            islandLocations.add(new Location(Bukkit.getWorlds().get(0), Integer.parseInt(splitted[0]),
                    Integer.parseInt(splitted[1]), Integer.parseInt(splitted[2])));
        }
    }

    public List<Location> getSpawnLocations() { return spawnLocations; }
    public Location getLobbyLocation() { return lobbyLocation; }
    public List<Location> getIslandLocations() { return islandLocations; }
    public List<Location> getLastLocations() { return lastSpawnLocations; }




}
